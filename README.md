# Terraform Cloud API driven Run from GitLab CI/CD
This repo uses GitLab CI/CD and Terraform Cloud API driven Run to deploy a compute instance in Google Cloud Platform. A Run is created in Terraform Cloud (TFC) from GitLab CI/CD. 
- For the CLI Driven Run example please see the [gl-terraform-gcp-compute-test](https://gitlab.com/kawsark/gl-terraform-gcp-compute-test) repo.

By default, this configuration provisions a single compute instance from image `ubuntu-os-cloud/ubuntu-1804-lts` with machine type `n1-standard-1` in the `us-east1-a` zone. The server quantity, OS image, machine type, GCP zone and region can all be changed as needed. Please see the [variables.tf](variables.tf) file for all defined terraform variables.

This repo has two Walk through guides:
- Walkthrough part 1 to implement the automated IaC provisioning workflow using GitLab CI/CD and Terraform Cloud. 
- Walkthrough part 2 to implement temporary GCP credentials from Vault and perform automated Sentinel policy checks.


## Walkthrough - part 1

**Pre-requisites:**
- TFC account and an API Token. 
  - You can sign up for a free account at [https://app.terraform.io](https://app.terraform.io).
  - Please see our documentation for creating a User [API token](https://www.terraform.io/docs/cloud/users-teams-organizations/users.html#api-tokens)
- A GitLab project/repository on [gitlab.com](https://www.gitlab.com) or a GitLab Enterprise server.
- A GCP Project where a VM can be built. Please see [creating a Project](https://cloud.google.com/resource-manager/docs/creating-managing-projects#creating_a_project) on GCP.
- GCP service account credentials (.json file). Please see [creating service account keys](https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys)

Please clone this repo to your GitLab account

### Create a GitLab Runner Docker image (Optional)
The Docker image used for the GitLab Runner in this demo has already made public in Dockerhub as [gitlab-ruby-curl](https://cloud.docker.com/repository/docker/kawsark/gitlab-ruby-curl). This container image is capable of performing an API driven run using `curl` and `jq`.

You can optionally customize a GitLab Runner image using included example [Dockerfile](scripts/Dockerfile). Below are the steps to create a custom image and upload to a container registry.
```
cd scripts
# Customize Dockerfile as needed
docker build -t <your_docker_username>/gitlab-ruby-curl:0.0.1 .
docker login
docker push <your_docker_username>/gitlab-ruby-curl:0.0.1
```

### Register the GitLab Runner
Register the GitLab Runner using GitLab.com UI as below.
- Click on Settings CI / CD Runners
  - Disable Shared Runners 
  - Install a Runner for this repo either locally, or in GKE. You could also install Runners in both places.
    - Host GitLab Runners on GKE: Please see [gke-runners.md](gke-runners.md) steps.
    - Host GitLab Runners locally: Please see [local-runners.md](local-runners.md) steps.
  
Please see [Install GitLab Runner](https://docs.gitlab.com/runner/install/) documenation to learn more about how to install Runners.

### Set GitLab variables
Please set GitLab variables via API or UI (project name > Settings > CI/CD > Variables).
  1. Transform the Google service account credentials .json file using the steps in [transforming GOOGLE_CREDENTIALS - API](transform_gcp_creds-api.md).
  1. Set transformed `GOOGLE_CREDENTIALS`
  1. Set `TFC_ADDR` to `app.terraform.io`, or your Private Terraform Enterprise Server hostname.
  1. Set `TFC_WORKSPACE` to your Terraform Cloud Workspace name
  1. Set `TFE_TOKEN` as your TFC API token. Note: this should be a User token, or a Team token with admin permission on the TFC Workspace (see [API Tokens](https://www.terraform.io/docs/cloud/users-teams-organizations/api-tokens.html#access-levels) for more information).
  1. Set `TFC_ORG` to your Terraform Cloud Organization name
  1. Set `gcp_project` to your GCP project name
  1. Set `gcp_region` to desired GCP region

### Trigger GitLab CI/CD Pipeline
Trigger the GitLab CI/CD Pipeline as below.
  1. Push a commit of your forked repo
  1. In GitLab.com project page, click CI/CD >> Pipelines >> “Run Pipeline”
  1. Using the GitLab CI/CD API: This method requires you to create a trigger token, and send a POST request to project specific endpoint. Please see [adding a new trigger token](https://docs.gitlab.com/ee/ci/triggers/#adding-a-new-trigger) for details - you can get an easy curl command to copy/paste.

## Walkthrough - part 2

**Pre-requisites:**
- You will need a Vault server with administrative access. If you don’t have one already, you can deploy one using our [Getting Started guide](https://learn.hashicorp.com/vault/getting-started/install). Your Vault token should have an admin policy which can create and configure Auth Methods (see [example-vault-admin-policy.hcl](https://gist.github.com/kawsark/4cdb66093d6206d9e036ecd1294e6509)).
```
export VAULT_ADDR=https://vault.example.org:8200
export VAULT_TOKEN=<admin-or-root-token>
vault status
vault token lookup
```

- The GitLab Runner should be able to reach Vault. Please test this by running the following command from the server where GitLab Runner is deployed.
```
curl -kv https://<vault_server>:8200/v1/sys/health
```
The above health checks should come back with “HTTP/2 200”.

- Destroy the previously created Terraform Cloud Workspace from the UI. 
  - Click on the Workspace >> my-gcp-gitlab-pipeline >> Settings >> Destruction and Deletion
  - Click "Queue Destroy Plan" button, then Confirm and Apply. 
  - Check the Apply logs to ensure your infrastructure was destroyed as requested.

### Vault setup
We have included a `vault_setup.sh` script to configure Vault with the GCP secrets engine needed for part 2.
```
# Setup environment
export VAULT_ADDR="https://vault.example.org:8200"
export VAULT_TOKEN="admin-or-root-token"
export GOOGLE_CREDENTIALS_PATH="/path/to/gcp-service-account.json"

# Clone repo and adjust permissions
git clone https://gitlab.com/kawsark/gitlabci-terraform-cloud-demo.git
cd gitlabci-terraform-cloud-demo/scripts
chmod +x vault_setup.sh

# Execute scripts
./vault_setup.sh
```
The script will provide a Vault token that will be used in the next section.

### GitLab setup
Please set GitLab variables via API or UI (project name > Settings > CI/CD > Variables).
  1. Add a `VAULT_ADDR` variable where your Vault server is accessible (including port # - default 8200)
  1. Add a `SECRETS_PATH` variable where the credentials can be retrieved from. In our example we are using `gcp/key/gitlab`
  1. Add the `VAULT_TOKEN` that you created in the previous section.
  1. Add `machine_type` to be `n1-standard-2`

### Terraform Cloud setup for Sentinel policy-as-code
We have included a `sentinel_setup.sh` script to configure TFC with a Sentinel Policy Set and Policy Set version.
```
# Assuming you are already in gitlabci-terraform-cloud-demo/scripts directory
export TFC_TOKEN=<your-terraform-cloud-token>
export TFC_ORG=<your-terraform-cloud-organization>

chmod +x sentinel_setup.sh
./sentinel_setup.sh
```

### Trigger Pipeine
To trigger the pipeline, please uncomment the `get_credentials` stage from `.gitlab-ci.yml` file, commit the changes, and push to the master branch. The top of your .gitlab-ci.yml file should look as below.
```
stages:
  - create_workspace
# Uncomment get_credentials to fetch temporary credentials from Vault
  - get_credentials
  - setup_workspace
  - run

before_script:
  - >
    export workspace_id=$(curl -s --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id)
  - echo "Workspace ID is ${workspace_id}"

# Uncomment the get_credentials job to fetch temporary credentials from Vault
get_credentials:
  stage: get_credentials
  script:
<OMITTED>
  - rm -f temp_creds
  tags:
    - curl
```

Save and commit these changes, then push the commit to your master branch.
```
git add .gitlab-ci.yml
git commit “Added get_credentials stage”
git push origin master.
```

At this point you should be able to see the Pipeline running from GitLab UI project page: CI / CD >> Pipelines.

## Clean up steps
1. If you used Vault to generate temporary credentials, please perform the following steps. Otherwise you can go to step 2.
  1. Terraform Cloud UI: Workspace >> my-gcp-gitlab-pipeline >> Variables. 
  1. Update the value of GOOGLE_CREDENTIALS with your original Google service account .json file contents.
  1. Run the command `vault secrets disable gcp` from a terminal after exporting `VAULT_ADDR` and `VAULT_TOKEN`.

1. Run the destroy.sh script after setting TFC_TOKEN and TFC_ORG as below. 
```
export TFC_TOKEN=<root-or-admin-token>
export TFC_ORG=<your-tfc-organization>
cd scripts
./destroy.sh
```
Please visit the Run URL provided by the script to ensure the destroy was completed. 

## Testing terraform configuration locally
Please see [local.md](local.md) to test this terraform configuration locally without using TFC or GitLab.
